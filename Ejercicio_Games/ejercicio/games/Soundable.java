package ejercicio.games;

interface Soundable {
	void playMusic(String song);
	
	default void setGameName(String name) {
		System.out.println(name);
	}

}
