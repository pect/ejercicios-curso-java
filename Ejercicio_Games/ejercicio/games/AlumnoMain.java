package ejercicio.games;

import java.util.ArrayList;
import java.util.List;

public class AlumnoMain {
	
	public static void main(String[] args) {
		List<Alumno> alumnos = new ArrayList<Alumno>();
		for (int i = 97, y = 1; i < 117; i++, y++) {
			Alumno alumno = new Alumno();
			alumno.setNombre("Alumno" + Character.toString((char) i));
			alumno.setApellidoMaterno("ApellidoM_" + Character.toString((char) i));
			alumno.setApellidoPaterno("ApellidoP_" + Character.toString((char) i));
			alumno.setNombreCurso("Curso" + y);
			alumnos.add(alumno);
		}
		alumnos.stream().forEach(System.out::println);
		System.out.println("\nNúmero de alumnos: " + alumnos.stream().count());
		System.out.println("\nAlumnos con nombre que terminan en a");
		alumnos.stream().filter(alumno -> alumno.getNombre().endsWith("a")).forEach(System.out::println);
		System.out.println("\nPrimeros 5 alumnos");
		alumnos.stream().limit(5).forEach(System.out::println);
	}

}
