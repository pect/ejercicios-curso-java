package ejercicio.games;

import java.util.Optional;

interface Playeable {
	void walk(double x, double y);
	
	default void setGameName(String name) {
		System.out.println(Optional.ofNullable(name).orElseThrow(() -> {throw new RuntimeException();}));
	}
}
