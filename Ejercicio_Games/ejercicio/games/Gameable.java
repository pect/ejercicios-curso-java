package ejercicio.games;

interface Gameable {
	void startGame();
	
	default void setGameName(String name) {
		System.out.println(name);
	}

}
