package ejercicio.games;

import java.util.Optional;

public class PrincipalGame {
	static String song = "";
	static String gameName = "Nombre Juego 123";
	static double playerPosx = 0;
	static double playerPosy = 0;
	
	public static void main(String[] args) {
		Playeable playeable = (px, py) -> {
			++px;
			++py;
			System.out.println(px);
			System.out.println(py);
		};
		playeable.walk(playerPosx, playerPosy);
		Gameable gameable = () -> {
			playeable.setGameName(gameName);
		};
		gameable.startGame();
		Soundable soundable = (songName) -> {
			song = Optional.ofNullable(songName).orElse("Sin titulo");
		};
		soundable.playMusic(null);
		System.out.println(song);
	}
}
